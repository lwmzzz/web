package com.lwm.web.config;

import com.lwm.web.common.Constants;
import com.lwm.web.interceptor.AdminLoginInterceptor;
import com.lwm.web.interceptor.ShopCartNumberInterceptor;
import com.lwm.web.interceptor.UserLoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Administrator
 * @date 2020/1/5
 */
@Configuration
public class BbWebMvcConfigurer implements WebMvcConfigurer {
    @Autowired
    private AdminLoginInterceptor adminLoginInterceptor;
    @Autowired
    private UserLoginInterceptor userLoginInterceptor;
    @Autowired
    private ShopCartNumberInterceptor shopCartNumberInterceptor;
   /** @Override
    public  void addViewControllers(ViewControllerRegistry registry){
        registry.addViewController("/index.html").setViewName("/front/index");
        registry.addViewController("/login.html").setViewName("/front/login");
        registry.addViewController("/register.html").setViewName("/front/register");
        registry.addViewController("/shopcart.html").setViewName("/front/shopcart");
        registry.addViewController("/userinfo.html").setViewName("/front/userinfo");
    }**/
    @Override
    public  void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(adminLoginInterceptor)
                .addPathPatterns("/admin/**")
                .excludePathPatterns("/admin/login")
                .excludePathPatterns("/admin/dist/**")
                .excludePathPatterns("/admin/plugins/**");
        registry.addInterceptor(shopCartNumberInterceptor)
                .excludePathPatterns("/admin/**")
                .excludePathPatterns("/register")
                .excludePathPatterns("/login")
                .excludePathPatterns("/logout");
        registry.addInterceptor(userLoginInterceptor)
                .excludePathPatterns("/admin/**")
                .excludePathPatterns("/register")
                .excludePathPatterns("/login")
                .excludePathPatterns("/logout")
                .addPathPatterns("/goods/detail/**")
                .addPathPatterns("/shop-cart")
                .addPathPatterns("/shop-cart/**")
                .addPathPatterns("/saveOrder")
                .addPathPatterns("/orders")
                .addPathPatterns("/orders/**")
                .addPathPatterns("/personal")
                .addPathPatterns("/personal/updateInfo")
                .addPathPatterns("/selectPayType")
                .addPathPatterns("/payPage");
    }
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/upload/**").addResourceLocations("file:" + Constants.FILE_UPLOAD_DIC);
        registry.addResourceHandler("/goods-img/**").addResourceLocations("file:" + Constants.FILE_UPLOAD_DIC);
    }
}
