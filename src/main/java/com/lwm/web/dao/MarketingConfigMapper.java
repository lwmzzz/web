package com.lwm.web.dao;

import com.lwm.web.bean.MarketingConfig;
import com.lwm.web.bean.MarketingConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MarketingConfigMapper {
    int countByExample(MarketingConfigExample example);

    int deleteByExample(MarketingConfigExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketingConfig record);

    int insertSelective(MarketingConfig record);

    List<MarketingConfig> selectByExample(MarketingConfigExample example);

    MarketingConfig selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketingConfig record, @Param("example") MarketingConfigExample example);

    int updateByExample(@Param("record") MarketingConfig record, @Param("example") MarketingConfigExample example);

    int updateByPrimaryKeySelective(MarketingConfig record);

    int updateByPrimaryKey(MarketingConfig record);
}