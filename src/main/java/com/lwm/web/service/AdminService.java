package com.lwm.web.service;

import com.lwm.web.bean.Admin;

/**
 * @author Administrator
 * @date 2020/1/5
 */
public interface AdminService {
    Admin login(String userName, String password);

    Admin getUserDetailById(Integer loginUserId);


    Boolean updatePassword(Integer loginUserId, String originalPassword, String newPassword);
}
