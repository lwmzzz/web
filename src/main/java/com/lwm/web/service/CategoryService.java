package com.lwm.web.service;

import com.lwm.web.bean.Category;
import com.lwm.web.controller.vo.CategoryVO;
import com.lwm.web.utils.PageQueryUtil;
import com.lwm.web.utils.PageResult;

import java.util.List;

/**
 * @author Administrator
 * @date 2020/1/5
 */
public interface CategoryService {

    /**
     * 后台分页
     *
     * @param pageUtil
     * @return
     */
    PageResult getCategorisPage(PageQueryUtil pageUtil);

    String saveCategory(Category goodsCategory);

    String updateGoodsCategory(Category goodsCategory);

    Category getGoodsCategoryById(Long id);

    Boolean deleteBatch(Integer[] ids);

    /**
     * 返回分类数据(首页调用)
     *
     * @return
     */
    List<CategoryVO> getCategoriesForIndex();

    /**
     * 返回分类数据(搜索页调用)
     *
     * @param categoryId
     * @return
     */
    CategoryVO getCategoriesForSearch(Long categoryId);


}
