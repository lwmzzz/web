package com.lwm.web.service;

import com.lwm.web.bean.Carousel;
import com.lwm.web.controller.vo.CarouselVO;

import java.util.List;

/**
 * @author Administrator
 * @date 2020/1/5
 */
public interface CarouselService {
    List<Carousel> getCarouselPage(int pageNum, int pageSize);
    String saveCarousel(Carousel carousel);

    String updateCarousel(Carousel carousel);

    Carousel getCarouselById(Integer id);

    Boolean deleteBatch(Integer[] ids);

    /**
     * 返回固定数量的轮播图对象(首页调用)
     *
     * @param number
     * @return
     */
    List<CarouselVO> getCarouselsForIndex(int number);
}
