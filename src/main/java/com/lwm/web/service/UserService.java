package com.lwm.web.service;


import com.lwm.web.utils.PageQueryUtil;
import com.lwm.web.utils.PageResult;

import javax.servlet.http.HttpSession;

/**
 * @author Administrator
 * @date 2020/1/5
 */
public interface UserService {
    PageResult getUserPage(PageQueryUtil pageUtil);
    /**
     * 用户注册
     *
     * @param loginName
     * @param password
     * @return
     */
    String register(String loginName, String password);
    /**
     * 用户登录
     *
     * @param loginName
     * @param password
     * @return
     */
    String login(String loginName, String password, HttpSession httpSession);

    /**
     * 用户禁用与解除禁用(0-未锁定 1-已锁定)
     *
     * @param ids
     * @param lockStatus
     * @return
     */
    Boolean lockUsers(Integer[] ids, int lockStatus);
}
