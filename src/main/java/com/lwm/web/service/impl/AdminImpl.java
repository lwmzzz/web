package com.lwm.web.service.impl;

import com.lwm.web.bean.Admin;
import com.lwm.web.bean.AdminExample;
import com.lwm.web.dao.AdminMapper;
import com.lwm.web.service.AdminService;
import com.lwm.web.utils.SHA512;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @date 2020/1/5
 */
@Service
public class AdminImpl implements AdminService {
    @Resource
    private AdminMapper adminMapper;
    @Override
    public Admin login(String userName, String password) {
        String passwordSHA512 = SHA512.getSHA512(password);
        AdminExample adminExample = new AdminExample();
        adminExample.createCriteria().andIdEqualTo(Integer.parseInt(userName));
        Admin admin = adminMapper.selectByExample(adminExample).get(0);
        if(admin.getPassword().equalsIgnoreCase(passwordSHA512)){
            return  admin;
        }
        else{
            return null;
        }
    }

    @Override
    public Admin getUserDetailById(Integer loginUserId) {
        return adminMapper.selectByPrimaryKey(loginUserId);
    }

    @Override
    public Boolean updatePassword(Integer loginUserId, String originalPassword, String newPassword) {
    Admin admin = adminMapper.selectByPrimaryKey(loginUserId);
    if(admin!=null){
        String originalPasswordSHA512 = SHA512.getSHA512(originalPassword);
        String newPasswordSHA512 = SHA512.getSHA512(newPassword);
        if(originalPasswordSHA512.equals(admin.getPassword())){
            admin.setPassword(newPasswordSHA512);
            if(adminMapper.updateByPrimaryKeySelective(admin)>0){
                return  true;
            }
        }
    }
        return  false;
    }


}
