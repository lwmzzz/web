package com.lwm.web.service.impl;

import com.lwm.web.bean.Category;
import com.lwm.web.controller.vo.CategoryVO;
import com.lwm.web.dao.CategoryMapper;
import com.lwm.web.service.CategoryService;
import com.lwm.web.utils.PageQueryUtil;
import com.lwm.web.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @date 2020/1/5
 */
@Service
public class CategoryImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public PageResult getCategorisPage(PageQueryUtil pageUtil) {
        return null;
    }

    @Override
    public String saveCategory(Category goodsCategory) {
        return  null;
    }

    @Override
    public String updateGoodsCategory(Category goodsCategory) {
        return null;
    }

    @Override
    public Category getGoodsCategoryById(Long id) {
        return null;
    }

    @Override
    public Boolean deleteBatch(Integer[] ids) {
        return null;
    }

    @Override
    public List<CategoryVO> getCategoriesForIndex() {
        return null;
    }

    @Override
    public CategoryVO getCategoriesForSearch(Long categoryId) {
        return null;
    }
}
