package com.lwm.web.service.impl;

import com.github.pagehelper.PageHelper;
import com.lwm.web.bean.Carousel;
import com.lwm.web.bean.CarouselExample;
import com.lwm.web.common.ServiceResultEnum;
import com.lwm.web.controller.vo.CarouselVO;
import com.lwm.web.dao.CarouselMapper;
import com.lwm.web.service.CarouselService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @date 2020/1/5
 */
@Service
public class CarouselServiceImpl implements CarouselService {

    private CarouselMapper carouselMapper;

    @Override
    public List<Carousel> getCarouselPage(int pageNum, int pageSize) {
       PageHelper.startPage(pageNum,pageSize);
       CarouselExample example = new CarouselExample();
       List<Carousel> list = carouselMapper.selectByExample(example);
       return  list;
    }

    @Override
    public String saveCarousel(Carousel carousel) {
        if(carouselMapper.insertSelective(carousel)>0){
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public String updateCarousel(Carousel carousel) {
        Carousel temp = carouselMapper.selectByPrimaryKey(carousel.getId());
        if (temp == null) {
            return ServiceResultEnum.DATA_NOT_EXIST.getResult();
        }
        temp.setRank(carousel.getRank());
        temp.setRedirectUrl(carousel.getRedirectUrl());
        temp.setUpdateTime(new Date());
        if(carouselMapper.updateByPrimaryKeySelective(temp)>0){
            return ServiceResultEnum.SUCCESS.getResult();
        }

        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public Carousel getCarouselById(Integer id) {
        return carouselMapper.selectByPrimaryKey(id);
    }

    @Override
    public Boolean deleteBatch(Integer[] ids) {
        return null;
    }

    @Override
    public List<CarouselVO> getCarouselsForIndex(int number) {
        List<CarouselVO> carouselVOList = new ArrayList<>(number);

        //List<Carousel> carousels = carouselMapper.selectByExample();
        return null;
    }
}
