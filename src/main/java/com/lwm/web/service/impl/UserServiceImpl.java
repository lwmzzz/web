package com.lwm.web.service.impl;


import com.lwm.web.bean.User;
import com.lwm.web.bean.UserExample;
import com.lwm.web.common.Constants;
import com.lwm.web.common.ServiceResultEnum;
import com.lwm.web.controller.vo.UserVO;
import com.lwm.web.dao.UserMapper;
import com.lwm.web.service.UserService;
import com.lwm.web.utils.BeanUtil;
import com.lwm.web.utils.PageQueryUtil;
import com.lwm.web.utils.PageResult;
import com.lwm.web.utils.SHA512;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

/**
 * @author Administrator
 * @date 2020/1/5
 */
@Service
public class UserServiceImpl implements UserService {

    private UserMapper userMapper;
    public  void setUserMapper(UserMapper userMapper){
        this.userMapper=userMapper;
    }
    @Override
    public PageResult getUserPage(PageQueryUtil pageUtil) {
        return null;
    }

    @Override
    public String register(String loginName, String password) {
        UserExample example = new UserExample();
        example.createCriteria().andPhoneEqualTo(loginName);
        User user= userMapper.selectByExample(example).get(0);
        if(user!=null){
            return ServiceResultEnum.SAME_LOGIN_NAME_EXIST.getResult();
        }
        User register = new User();
        register.setPhone(loginName);
        register.setPassword(SHA512.getSHA512(password));

        if(  userMapper.insert(register)>0){
            return ServiceResultEnum.SUCCESS.getResult();
        }
        else {
            return ServiceResultEnum.DB_ERROR.getResult();
        }
    }

    @Override
    public String login(String loginName, String password, HttpSession httpSession) {
        String passwrdSHA512 = SHA512.getSHA512(password);
        UserExample example  = new UserExample();
        example.createCriteria().andPhoneEqualTo(loginName);
        User user = userMapper.selectByExample(example).get(0);
       if(user!=null&&httpSession!=null){
         if(user.getPassword().equalsIgnoreCase(passwrdSHA512))  {
             if(user.getLockedFlag()==1){
                 return  ServiceResultEnum.LOGIN_USER_LOCKED.getResult();
             }
             else {
                 UserVO userVO = new UserVO();
                 BeanUtil.copyProperties(user,userVO);
                 httpSession.setAttribute(Constants.MALL_USER_SESSION_KEY,userVO);
                 return  ServiceResultEnum.SUCCESS.getResult();
             }
            // return ServiceResultEnum.LOGIN_ERROR.getResult(); }


       }
        return ServiceResultEnum.LOGIN_ERROR.getResult();
    }


        return ServiceResultEnum.LOGIN_ERROR.getResult();
}
    @Override
    public Boolean lockUsers(Integer[] ids, int lockStatus) {
        return  null;
    }
}
