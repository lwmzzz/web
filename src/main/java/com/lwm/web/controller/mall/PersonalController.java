package com.lwm.web.controller.mall;

import com.lwm.web.common.Constants;
import com.lwm.web.common.ServiceResultEnum;
import com.lwm.web.service.UserService;
import com.lwm.web.utils.Result;
import com.lwm.web.utils.ResultGenerator;
import com.lwm.web.utils.SHA512;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Administrator
 * @date 2020/1/5
 */
@Controller
public class PersonalController {
    @Resource
    private UserService userService;
    @GetMapping({"/login","login.html"})
    public String loginPage(){return "mall/login";}
    @GetMapping("/personal")
    public String personalPage(HttpServletRequest request,
                               HttpSession httpSession) {
        request.setAttribute("path", "personal");
        return "mall/userinfo";
    }

    @GetMapping("/logout")
    public String logout(HttpSession httpSession) {
        httpSession.removeAttribute(Constants.MALL_USER_SESSION_KEY);
        return "mall/login";
    }

    @GetMapping({"/register", "register.html"})
    public String registerPage() {
        return "mall/register";
    }

    @GetMapping("/personal/addresses")
    public String addressesPage() {
        return "mall/addresses";
    }
    @PostMapping("/login")
    @ResponseBody
        public Result login(@RequestParam("loginName") String loginName,@RequestParam("password") String password,HttpSession httpSession){

        if(StringUtils.isEmpty(loginName)){
            return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_NAME_NULL.getResult());
        }
        if (StringUtils.isEmpty(password)) {
            return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
        }
        String loginResult = userService.login(loginName, SHA512.getSHA512(password),httpSession);
        //登录成功
        if (ServiceResultEnum.SUCCESS.getResult().equals(loginResult)) {
            return ResultGenerator.genSuccessResult();
        }
        //登录失败
        return ResultGenerator.genFailResult(loginResult);
    }
    @PostMapping("/register")
    @ResponseBody
    public Result register(@RequestParam("loginName") String loginName,
                           @RequestParam("password") String password,
                           HttpSession httpSession) {
            if (StringUtils.isEmpty(loginName)) {
                return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_NAME_NULL.getResult());
            }
            if (StringUtils.isEmpty(password)) {
                return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
            }
            String registerResult = userService.register(loginName,password);
        //注册成功
        if (ServiceResultEnum.SUCCESS.getResult().equals(registerResult)) {
            return ResultGenerator.genSuccessResult();
        }
        //注册失败
        return ResultGenerator.genFailResult(registerResult);
    }

}
