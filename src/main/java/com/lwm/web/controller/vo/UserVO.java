package com.lwm.web.controller.vo;

import java.io.Serializable;

/**
 * @author Administrator
 * @date 2020/1/5
 */
public class UserVO implements Serializable {
    private Integer id;

    private String phone;

    private String username;

    private String address;

    private  int shopcartitemcount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getShopcartitemcount() {
        return shopcartitemcount;
    }

    public void setShopcartitemcount(int shopcartitemcount) {
        this.shopcartitemcount = shopcartitemcount;
    }
}
