package com.lwm.web.controller.vo;

/**
 * @author Administrator
 * @date 2020/1/5
 */
public class CategoryVO {
    private  Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    private  String categoryName;
}
